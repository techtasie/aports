# Contributor: Zach DeCook <zachdecook@librem.one>
# Maintainer: Zach DeCook <zachdecook@librem.one>
pkgname=hyprcursor
pkgver=0.1.5
pkgrel=0
pkgdesc="The hyprland cursor format, library and utilities."
url="https://github.com/hyprwm/Hyprcursor"
arch="all"
license="BSD-3-Clause"
source="https://github.com/hyprwm/Hyprcursor/archive/v$pkgver/Hyprcursor-v$pkgver.tar.gz"
options="!check" # tests are broken
makedepends="
	cairo-dev
	cmake
	hyprlang
	librsvg-dev
	libzip-dev
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-util"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None
	cmake --build build
}

check() {
	cd build
	# Test won't work unless environment has a hyprcursor that can be loaded.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
	install -Dm644 -t "$pkgdir"/usr/share/doc/hyprcursor docs/*
	install -Dm644 hyprcursor-util/README.md "$pkgdir"/usr/share/doc/hyprcursor/README.md
}

util() {
	# TODO: Depend on xcur2png for -x option
	amove usr/bin/hyprcursor-util
}


sha512sums="
3092b6773a34fa1cab10f83fa48274d79a50cd373dfe8a89d0170a78767a66b5e0cbe57cd1b86bc4bb85371c23ac7425f59e61fb50723a895fef4e7f6039fdfc  Hyprcursor-v0.1.5.tar.gz
"
