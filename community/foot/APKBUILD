# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=foot
pkgver=1.17.0
pkgrel=0
pkgdesc="Fast, lightweight and minimalistic Wayland terminal emulator"
url="https://codeberg.org/dnkl/foot"
license="MIT"
arch="all"
depends="ncurses-terminfo"
makedepends="
	cage
	font-dejavu
	fcft-dev
	fontconfig-dev
	freetype-dev
	libxkbcommon-dev
	meson
	ncurses
	pixman-dev
	scdoc
	tllist-dev
	utf8proc-dev
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-extra-terminfo:_extra_terminfo:noarch
	$pkgname-themes:_themes:noarch
	"
source="
	$pkgname-$pkgver.tar.gz::https://codeberg.org/dnkl/foot/archive/$pkgver.tar.gz
	"
options="!check" # ran during profiling
builddir="$srcdir/foot"

build() {
	export CFLAGS="$CFLAGS -O3" # -O3 as the package is intended to use it
	export CXXFLAGS="$CXXFLAGS -O3"
	export CPPFLAGS="$CPPFLAGS -O3"

	abuild-meson \
		-Db_pgo=generate \
		-Db_lto=true \
		-Dterminfo-base-name=foot-extra \
		-Dutmp-backend=none \
		. output
	meson compile -C output

	ninja -C output test
	./pgo/full-headless-cage.sh . output

	meson configure -Db_pgo=use output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_extra_terminfo() {
	pkgdesc="$pkgdesc (extra terminfo data)"

	amove /usr/share/terminfo/f
}

_themes() {
	pkgdesc="$pkgdesc (color schemes)"

	amove /usr/share/foot/themes
}

sha512sums="
15f31a0dfeb681111bc760ad610740c682c475971520b601f01d48e61a8ec0bf37af10eadca79c88bbd60133eb1cf1f5efd99c9c0db571256ba3f3fc861fe9db  foot-1.17.0.tar.gz
"
